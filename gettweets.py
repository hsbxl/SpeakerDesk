import tweepy
import wget
import shutil
import os
import config


if os.path.exists(config.imgdir):
    shutil.rmtree(config.imgdir)

if not os.path.exists(config.imgdir):
    os.makedirs(config.imgdir)

auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.access_token_key, config.access_token_secret)
api = tweepy.API(auth)

for tweet in tweepy.Cursor(api.search, q=config.search, count=20, result_type="recent", include_entities=True).items():
    if 'media' in tweet.entities:
        wget.download(tweet.entities["media"][0].get("media_url"), config.imgdir)
